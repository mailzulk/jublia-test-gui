// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import Datetime from 'vue-datetime'
// import DatetimePicker from 'vuetify-datetime-picker'

import 'vuetify/dist/vuetify.min.css'
import 'vue-datetime/dist/vue-datetime.css'
// import 'vuetify-datetime-picker/src/stylus/main.styl'

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueMomentJS, moment)
Vue.use(Datetime)
// Vue.use(DatetimePicker)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
