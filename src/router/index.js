import Vue from 'vue'
import Router from 'vue-router'
import Email from '@/components/Email'
import NewEmail from '@/components/NewEmail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Email',
      component: Email
    },
    {
      path: '/create',
      name: 'New Email',
      component: NewEmail
    }
  ]
})
